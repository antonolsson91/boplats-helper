 /* TODO: Idé
  * när man klickar på chrome ikonen skall man få upp en popup som visar hur många ködagar, meddelanden osv man har
  * kanske också kunna ansöka direkt från samma popup
  * alternativt en snabbmeny med länkar till t.ex favoritsökningar, fakturor, meddelanden, profil osv
  */
jQuery.noConflict();
(function( $ ) {
  console.success = console.log;

  $(function() {
    $( document ).ready(function() {

      var body;
      var INLOGGAD = false;
      var LOGGAR_IN = false;
      var LOADER = `<img style="margin-left: 5px; width: 15px;" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOng9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZpZXdCb3g9IjAgMCAxMDAgMTAwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJ4TWluWU1pbiBub25lIj48Zz48cGF0aCBpZD0icCIgZD0iTTMzIDQyYTEgMSAwIDAgMSA1NS0yMCAzNiAzNiAwIDAgMC01NSAyMCIvPjx1c2UgeDpocmVmPSIjcCIgdHJhbnNmb3JtPSJyb3RhdGUoNzIgNTAgNTApIi8+PHVzZSB4OmhyZWY9IiNwIiB0cmFuc2Zvcm09InJvdGF0ZSgxNDQgNTAgNTApIi8+PHVzZSB4OmhyZWY9IiNwIiB0cmFuc2Zvcm09InJvdGF0ZSgyMTYgNTAgNTApIi8+PHVzZSB4OmhyZWY9IiNwIiB0cmFuc2Zvcm09InJvdGF0ZSgyODggNTAgNTApIi8+PGFuaW1hdGVUcmFuc2Zvcm0gYXR0cmlidXRlTmFtZT0idHJhbnNmb3JtIiB0eXBlPSJyb3RhdGUiIHZhbHVlcz0iMzYwIDUwIDUwOzAgNTAgNTAiIGR1cj0iMS44cyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiLz48L2c+PC9zdmc+" id="LOADER" alt="Laddar">`;
      var LOCKED = false;
      var LOCKED_TXT = "";
      var STATUS_TXT = "";
      var QUEUEDAYS = 0;
      var STORAGE = localStorage.getItem('search-history')

      function getSearchHistory(){ return JSON.parse(localStorage.getItem("search-history"));  }
      function setSearchHistory(arr){ return localStorage.setItem("search-history", JSON.stringify(arr.slice(0,5)) ) }
      console.log(`Search History:`)
      console.log( getSearchHistory() )

      function toast(str){Materialize.toast(str, 2000);}

      $(document).on('click','.search-history div',function(event){

        var str = $(event.target).attr("data-href"); //"https://nya.boplats.se/sok#itemtype=rum&filterrequirements=on&rent=9000&city=alla&rooms=&area=508A8CB406FE001F00030A60&508A8CB406FE001F00030A60=alla&508A8CB4FCA4001400031899=alla&508A8CB400A1001800031575=alla&54B00286892C0009000349A6=alla&508A8CB4044A002300035C4C=alla&53EC5D3E25A7000A0003704B=alla&508A8CB405A30025000303BA=alla&508A8CB406840026000312AF=alla&508A8CB4F981002B000339E4=alla&508A8CB4FBDC002E000345E7=alla&508A8CB4FD6E00300003D3DD=alla&5163DE62FA20000700035F18=alla&squaremeters=&objecttype=alla&objecttype-alla=on&moreoptionsvisible=false&moveindate=any&deposit=&objectproperties=&sortorder=startPublishTime-descending&listtype=imagelist";
        var res = str.match(/([.]*)\w+([.]*=)\w+/g);
        obj = {}
        for(x in res){
          let vals = res[x].split("=")
          let prop = vals[0]
          let val = vals[1]
          obj[prop] = val;
        }

        for(prop in obj){
          let oldval = $("#" + prop).val()
          let newval = obj[prop];
          let elem = $("#" + prop);
          // $("#" + prop).val( obj[prop] )
          console.log(elem);
          console.log(`old ${prop} = ${oldval} \n new ${prop} = ${newval}`);
        }
        event.preventDefault();
      });

      /*
      TODO: Kolla i meddelanden efter gatunamn som skall återpubliceras
      och skicka notifiering när dessa hittas som återpublicerade.


      */

      function searchRows( HISTORY ){

        let searchRows = "";

        for(i in HISTORY){
          // console.log(`Added new row ${i} of search history`)

          url = HISTORY[i].url
          val_boendetyp = HISTORY[i].val_boendetyp
          val_hyra_result = HISTORY[i].val_hyra_result
          val_kommun = HISTORY[i].val_kommun
          val_omrade = HISTORY[i].val_omrade
          val_typavlagenhet = HISTORY[i].val_typavlagenhet
          val_visabara = HISTORY[i].val_visabara

          let searchRow = `
          <tr class="search-history">
          <td colspan="4">
          <div data-href="${url}">
          <strong>${val_boendetyp}</strong><br>
          ${val_hyra_result}
          Kommun: ${val_kommun}<br>
          Område: ${val_omrade}<br>
          Typ av lägenhet: ${val_typavlagenhet}<br>
          ${val_visabara}
          </div>
          </td>
          </tr>`;

          searchRows += searchRow;
        }

        let currentHTML = $("#search-history tbody").html().length;

        function htmlDecode(value) {
          return $("<textarea/>").html(value).text(searchRows);
        }

        let test = htmlDecode( searchRows );
        var parser = new DOMParser;
        var dom = parser.parseFromString('<!doctype html><body>' + searchRows,'text/html');
        let htmlcontent = dom.body.textContent;

        // if(currentHTML == searchRows){console.success("yes")}
        // if(currentHTML == test){console.success("yes")}

        if(currentHTML != searchRows){
          $("#search-history tbody").html( searchRows );

          console.log(`%c${test[0].outerText.length}`,'background:rgb(165, 208, 145);')

          console.log(`%c${currentHTML}`,'background:rgb(202, 172, 242);')

        }
      }

      /*! TODO RESTRUCTURE */
      $.get("https://nya.boplats.se/minsida/profil",function(data){
      }).done( function( data ) {
        let _locked = data.includes("Låst - kan ändras igen om ")  //(Låst - kan ändras igen om 6 dagar)

        if( _locked ){
          LOCKED = true;
          LOCKED_TXT = $(data).find(".locked span.infotext").html();
        }

        try{
          QUEUEDAYS = parseInt($(data).find("td.leftsidebar .status p").first().html().replace("<b>Dagar hos Boplats:</b>","").trim());
          toast(`Du har ${QUEUEDAYS} ködagar`);

        }
        catch(e){

        }
      });

      if( $("body").html().includes("Du har fyllt i fel") )
      return;

      // boplatshelper menu
      $(".rightsidebar").first().prepend(BOPLATS_HELPER_HTML);

      if( !$("#objectlist").length ){

      } else {

        var target= $("#objectsearchform").get(0);
        var config = {childList: true, subtree: true};

        var Observer=new MutationObserver(function(mutations) {
          console.log(mutations);
          Observer.disconnect();
          onSearch();

          // Lägg till Ansök-knapp
          var annonsNum = $(".imageitem td a").length;
          console.log('Annonser i resultatet: ', annonsNum, $(".imageitem td a"))

          $(".imageitem td a").each( function(i, e){
            var $e = $(e);
            if( $e.html().includes("ansök direkt") ) return;

            const IS_UNAVAILABLE = ($e.find('img[src*="cross"]').length==0);
            const IS_APPLIED = ($e.find('img[src*="ansokt"]').length==0);
            const IS_QUESTION = ($e.find('img[src*="result_question"]').length==0);

            if( IS_UNAVAILABLE && IS_APPLIED && IS_QUESTION ){
              $e.append(`<div class="applybox boxy">ansök direkt</td>`);
              //$(this).find(".rent").parent().append();
            } else {
              console.log('En annons kunde ej sökas: ', $e.html())
            }
          });

          let strBas = $("body").find('img[alt="Du uppfyller basvillkoren"]').length;
          let strInte = $("body").find('img[alt="Du uppfyller inte basvillkoren"]').length;
          let strAnsokt = $("body").find('img[alt="Du har ansökt"]').length;

          $(".applytoall span").first().html( `(${strBas})` );
          $(".unavailable span").first().html( `(${strInte})` );
          $(".applied span").first().html( `(${strAnsokt})` );

          if ( $("body").html().includes("Lägenheten har basvillkor") )
          INLOGGAD = false;

          !$("body").html().includes("Du uppfyller basvillkoren")
          ? $(".applytoall").hide()
          : $(".applytoall").show();

          !$("body").html().includes("Du uppfyller inte basvillkoren")
          ? $(".unavailable").hide()
          : $(".unavailable").show();

          !$("body").html().includes("Du har ansökt")
          ? $(".applied").hide()
          : $(".applied").show();

          !$("body").find('a[href="https://nya.boplats.se/minsida/meddelande"]').is(":visible") //!$("body").html().includes('<a href="https://nya.boplats.se/minsida/meddelande">Meddelanden</a>')
          ? $("#read-all-messages").hide()
          : $("#read-all-messages").show();

          ($(".imageitem").length==0)
          ? $(".rangordning").hide()
          : $(".rangordning").show();

          $("#status_txt td").html(STATUS_TXT);

          searchRows( getSearchHistory() )

          Observer.observe(target,config);
          if( $("body").html().includes("Visa fler ") && $('.showmore').is(":visible") ){
            $(".showmore button").click();
            $('#statustext').append(`${new Date().getTime()} ${_frames()} Auto-klickade på .showmore<br>`)
          }
          // mutations.forEach(function(mutationRecord){          });
        });

        Observer.observe(target,config);

      }

      /*

      var searchTimer = setInterval(function() {
      if ( $("#objectlist").hasClass("withdata") ) {
      onSearch();
      clearInterval(searchTimer);
    }
  }, 200);

  */


  READALLMESSAGES : {

    let first = "";
    let boplatshelper_btns = `
    <div id="boplatshelper_btns" style="margin-bottom: 8px;">
    <button id='read-all-messages' style="margin: 3px;">Markera alla meddelanden som lästa</button>
    <button id="ladda-historik" style="margin: 3px;">Ladda Historik</button>
    </div>
    `;

    if(location.href == "https://nya.boplats.se/minsida/meddelande"){
      first = $("#meddelande h2").first();
    } else {
      first = $("#boplats_helper_html tbody tr td h4").first();
    }
    $(boplatshelper_btns).insertBefore(first);

    $(document).on("click", "#read-all-messages", function(){
      $.get("https://nya.boplats.se/minsida/meddelande",function(data){
        $( "#read-all-messages" ).append( LOADER );

      }).done( function( data ) {

        let x = 0;
        $(data).find(".unread").each(function(i){
          url = $(this).find("a[href*='meddelande']").attr("href");
          $.get(url,function(data){
            x++;
          }).done( function( data ){
            x--;
            if(x == 0){
              $( "#read-all-messages #LOADER" ).remove();
              $("a[href*='meddelande']").parent().parent().css("background-color","red");
              $("a[href*='meddelande']").parent().parent().fadeOut("slow");
              $( "#read-all-messages" ).fadeOut("slow");
            }
          });

        });


      });


    });
  }



  HISTORY:{

    $('head').append(`<link href='https://mottie.github.io/tablesorter/css/theme.blue.css' rel='stylesheet'>`);
    $('body').prepend(`<div id='history-holder'></div>`);
    $("#history-holder").hide();



    once = false;
    $(document).on('click', "#ladda-historik", function(){
      var arr = [];

      $("#history-holder").toggle();
      $("#history-table").toggle();
      $("#ladda-historik").toggleClass("toggled");

      if( $('#ladda-historik').hasClass("toggled") ){
        $('#ladda-historik').html('Göm Historik');
      } else {
        $('#ladda-historik').html('Visa Historik');
      }

      if(once === false){
        once=true;

        $( "#history-holder, #ladda-historik" ).append(`${LOADER}`);
        $( "#history-holder" ).append(`Historiken laddas från <a target="_blank" href="https://nya.boplats.se/minsida/ansokta/past/1hand">/minsida/ansokta/past/1hand</a>`);

        $.get( "https://nya.boplats.se/minsida/ansokta/past/1hand", function( data ) {

        })
        .done( function( data ) {

          $("#history-holder").append(`<table id="history-table"><thead>
          <th>Rangordning</th>
          <th>Ködagar</th>
          <th>Hyra</th>
          <th>Rum</th>
          <th>Area/Storlek</th>
          <th>Gata</th>
          <th>Karta</th>
          </thead><tbody></tbody></table>
          `);

          let history = $(data).find(".history").html();

          $(data).find(".history tr").each(function(i, e){
            var queuetime;

            var l = $(this).find(".leased").html(); if(l === null) return;
            var queuetime_re = /([0-9]{2,4})/i;
            try{queuetime = l.match(queuetime_re)[0];}catch(err){console.log(`Kötid hittades inte. ${err}`);return;}

            var d = $(this).find(".description").html();
            var gata = $(this).find(".description b").html(); gata = gata.replace(" .","");


            var rum_re=/([0-9]{1}) rum/;
            var area_re=/([0-9]{1,3}\.[0-9]{1}) m²/;
            var hyra_re=/([0-9]{1,4}) kr\/mån/;

            var rum,hyra,area="";
            try{rum  = d.match(rum_re)[1];  }catch(err){console.log(`Rum hittades inte. ${err}`);}
            try{hyra = d.match(hyra_re)[1]; }catch(err){console.log(`Hyra hittades inte. ${err}`);}
            try{area = d.match(area_re)[1]; }catch(err){console.log(`Area hittades inte. ${err}`);}

            arr[i] = {}

            arr[i].queuetime = queuetime;
            arr[i].hyra = hyra;
            arr[i].rum = rum;
            arr[i].area = area;
            arr[i].gata = gata.replace("1 1", "11");


          }); //each history

          function GetSomeDeferredStuff(){
            deferreds = []
            for(i in arr){
              let mapslink = "https://maps.googleapis.com/maps/api/geocode/json?address="+arr[i].gata+"+V%C3%A4stra+G%C3%B6taland&key=AIzaSyA5lPzxf4zAKXFZz7Uvreaefiqa7ITCm4Y";
              deferreds.push(

                $.ajax({
                  url: mapslink,
                  indexValue: i,
                  success: function(data) {
                    image_link(data, this.indexValue);

                    function image_link(data, i) {
                      try{
                        arr[i].formatted_address = data.results[0].formatted_address;
                        arr[i].statsdel = data.results[0].address_components[2].long_name;
                        arr[i].coords = data.results[0].geometry.location;
                      } catch(e){
                        console.info(e)
                      }
                    }
                  }
                })
              );
            }
            return deferreds;
          }


          var deferreds = GetSomeDeferredStuff()
          $.when.apply(null, deferreds).done(function(derpy) {
            // console.log("All done!");

            $("#history-holder #LOADER, #ladda-historik #LOADER").remove();


            for(i in arr){
              if(typeof arr[i].coords != "object" || typeof arr[i].coords.lat !== "number"){ console.info(arr[i]); continue; }

              let lat = arr[i].coords.lat;
              let lng = arr[i].coords.lng;

              let url = `https://maps.googleapis.com/maps/api/staticmap?`+
              `center=${lat},${lng}`+
              `&zoom=10&size=300x150&maptype=roadmap`+
              // `&markers=color:blue|label:A|${lat},${lng}`+
              // `&markers=color:green%7Clabel:G%7C40.711614,-74.012318`+
              // `&markers=color:red%7Clabel:C%7C40.718217,-73.998284`+
              `&key=AIzaSyA5lPzxf4zAKXFZz7Uvreaefiqa7ITCm4Y`;

              let url_ = `https://maps.googleapis.com/maps/api/streetview?size=400x400&location=${lat},${lng}`+
              // `&fov=90&heading=235&pitch=10`+
              `&key=AIzaSyA5lPzxf4zAKXFZz7Uvreaefiqa7ITCm4Y`;

              //  https://maps.googleapis.com/maps/api/staticmap?center=63.259591,-144.667969&zoom=6&size=400x400\
              // &markers=color:blue%7Clabel:S%7C62.107733,-145.541936&markers=size:tiny%7Ccolor:green%7CDelta+Junction,AK\
              // &markers=size:mid%7Ccolor:0xFFFF00%7Clabel:C%7CTok,AK"&key=AIzaSyA5lPzxf4zAKXFZz7Uvreaefiqa7ITCm4Y

              $(`<tr>
                <td>${i}</td>
                <td>${arr[i].queuetime}</td>
                <td>${arr[i].hyra}</td>
                <td>${arr[i].rum}</td>
                <td>${arr[i].area}</td>
                <td>
                ${arr[i].gata} (${arr[i].statsdel})
                </td>
                <td>
                <canvas id="${i}" data-statsdel="${arr[i].statsdel}" data-url="${url}" data-queuetime="${arr[i].queuetime}"></canvas>
                <img src="${url_}" alt="" />

                </td>

                </tr>`).appendTo("#history-table tbody");


              }


              $("#history-table canvas").each(function(e){
                var canvas = $(this)[ 0 ]
                var ctx = canvas.getContext("2d");
                ctx.fillStyle = "blue";
                ctx.textAlign = "center";
                var img = new Image;
                var statsdel = $(this).attr("data-statsdel")
                var queuetime = $(this).attr("data-queuetime")

                img.onload = function(){
                  ctx.drawImage(img,0,0); // Or at whatever offset you like
                  ctx.font = "14px Arial";
                  ctx.fillText(statsdel, canvas.width/2, canvas.height/2);

                  ctx.font = "12px Arial";
                  ctx.fillText(`${queuetime} ködagar`, canvas.width/2, canvas.height/2+14);
                };
                img.src = $(this).attr("data-url")

              });

              $('#history-table').tablesorter({theme: 'blue', widthFixed: true, widgets:['zebra'],sortList: [[0,0]]});

            });



          }); //$.get


        }//once

      }); //onclick ladda historik


    } //if document.URL


    body = $("body").html();

    //shortcut icon
    $("head").append(`<link type="image/jpeg" rel="shortcut icon" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAB2AAAAdgB+lymcgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAEhSURBVDiN5ZI/SwNBEMXfbLJ7AbETInedYm8lFjZ2IghJoR9AsFBJFdFWP0Eugq2phLCNiqKlXewCfgIxxwXFP43ExDO5sRL0shtIWl858/jNPHjAALHeV0FldmaQR9gWjdPCdJB5vUD0Xgt9tXdfQcbko+RA69XUnMzmSHAZIE88XnWoHToArqMubU3tdB6sHzyf747PO1mfBKoAeYlDyyrNtWZJ5ayAD26dgLANIG1J5sZEullWeSMAhEnm/lgJSbCYMANG0H8AEMAjAQjoMeOM4q91AI1hAS0Gii9P0Zq3GVSpJxYAXJqMpsLUpeANd+Wo/jPwiu2Ab5AP75wCGAe/m/IHQKBjcqR2l0pvfZEW0WV8+uGhvI05NWaLNLS+AfVkWFXrPM62AAAAAElFTkSuQmCC">`);


    function loggaIn(){
      $( "#login" ).trigger( "click" );
      $( "#login" ).trigger( "click" );
      $( "#login" ).append( LOADER );

      if(LOGGAR_IN) return false;

      setTimeout(function() {},1000);

      var loginTimer = setInterval(function() {
        LOGGAR_IN = true;

        if ( $("#username").val() !== "" ){
          $("#password").toggle();
          $("#password").attr("type", "text");


          if( $("#password").val() !== "" ){
            window.usr = $("#username").val();
            window.pwd = $("#password").val();
            console.log(window.usr, window.pwd);

            $("#password").attr("type", "password");
            $("#password").toggle();
            //run some other function

            var body = $("body").html();
            $.get( "https://nya.boplats.se/login/", function( data ){}).done( function( data ) {

              var usr = $("input[name='username']").val();
              var pwd = $("input[name='password']").val();
              var at  = $( data ).find('input[name*="authenticity_token"]').val();

              $.post( "https://nya.boplats.se/login/fragment", { username: usr, password: pwd, utf8:"✓", authenticity_token:at }, function( data ) {

              }).done( function( data ) {
                if( body && body.indexOf('Inloggning startad') == -1 ) {
                  $.get( "https://nya.boplats.se/minsida/", function( data ){}).done( function( data ) {
                    let userbox = $(data).find(".userbox").html();
                    $( "#LOADER" ).hide();
                    $(".userbox").html( userbox );
                    clearInterval(loginTimer);
                    $( "#popover" ).fadeOut();
                  });
                } else {
                  console.log('Inloggningen misslyckades');

                }
              });
            });

            // $( "#loginform" ).trigger( "submit" );





          }
        }
      }, 10000);

    }

    // reorder
    $($(".status")[1]).prependTo(".rightsidebar");

    // load settings
    //loadS();

    const FPS = 10 // 40 FPS
    var frames = 0;
    function _frames(){
      return `<div><span style="opacity:0.66;">${frames}</span>`;
    }
    var main = setInterval(function(){
      frames++;

      if(!INLOGGAD && !LOGGAR_IN){
        LOGGAR_IN = true;
        loggaIn();
      }

      if( $("body").html().includes("Inloggad som:") )
      INLOGGAD = true;

      if(INLOGGAD)
      $("#autologin").hide();

      // every 40 frames
      /*
      if( frames % 40 == 0){
      if( $("body").html().includes("Visa fler ") && $('.showmore').is(":visible") ){
      $(".showmore button").click();
      STATUS_TXT += `${_frames()} Auto-klickade på .showmore<br>`
    }
  }



  // Lägg till Ansök-knapp
  $(".imageitem td a").each( function(i, e){
  if( $(e).html().includes("ansök direkt") ) return 0;

  const IS_UNAVAILABLE = ($(this).find('img[src*="cross"]').length==0);
  const IS_APPLIED = ($(this).find('img[src*="ansokt"]').length==0);
  const IS_QUESTION = ($(this).find('img[src*="result_question"]').length==0);
  if( IS_UNAVAILABLE && IS_APPLIED && IS_QUESTION ){
  $(e).append(`<div class="applybox boxy">ansök direkt</td>`);
  //$(this).find(".rent").parent().append();
}
});

let strBas = $("body").find('img[alt="Du uppfyller basvillkoren"]').length;
let strInte = $("body").find('img[alt="Du uppfyller inte basvillkoren"]').length;
let strAnsokt = $("body").find('img[alt="Du har ansökt"]').length;

$(".applytoall span").first().html( `(${strBas})` );
$(".unavailable span").first().html( `(${strInte})` );
$(".applied span").first().html( `(${strAnsokt})` );

if ( $("body").html().includes("Lägenheten har basvillkor") )
INLOGGAD = false;

!$("body").html().includes("Du uppfyller basvillkoren")
? $(".applytoall").hide()
: $(".applytoall").show();

!$("body").html().includes("Du uppfyller inte basvillkoren")
? $(".unavailable").hide()
: $(".unavailable").show();

!$("body").html().includes("Du har ansökt")
? $(".applied").hide()
: $(".applied").show();

!$("body").find('a[href="https://nya.boplats.se/minsida/meddelande"]').is(":visible") //!$("body").html().includes('<a href="https://nya.boplats.se/minsida/meddelande">Meddelanden</a>')
? $("#read-all-messages").hide()
: $("#read-all-messages").show();

($(".imageitem").length==0)
? $(".rangordning").hide()
: $(".rangordning").show();

$("#status_txt td").html(STATUS_TXT);

searchRows( getSearchHistory() )
*/
}, 1000/FPS);





function onSearch() {


  /*
  * TODO: lägga till alla sökresultat (XX objekt) genom att posta form till https://nya.boplats.se/sok/fragment
  itemtype:1hand
  rent:
  city:alla
  rooms:
  area:508A8CB406FE001F00030A60
  508A8CB406FE001F00030A60:alla
  508A8CB4FCA4001400031899:alla
  508A8CB400A1001800031575:alla
  54B00286892C0009000349A6:alla
  508A8CB4044A002300035C4C:alla
  53EC5D3E25A7000A0003704B:alla
  508A8CB405A30025000303BA:alla
  508A8CB406840026000312AF:alla
  508A8CB4F981002B000339E4:alla
  508A8CB4FBDC002E000345E7:alla
  508A8CB4FD6E00300003D3DD:alla
  5163DE62FA20000700035F18:alla
  squaremeters:
  objecttype:alla
  moreoptionsvisible:false
  moveindate:any
  deposit:
  objectproperties:
  sortorder:startPublishTime-descending
  listtype:imagelist
  skip:30

  response består av <tr class="item imageitem">
  som kan läggas in direkt $('table#objectlist tbody tr[style*="display: table-row;"]').before()

  */

  // History

  docUrl = document.URL.replace(/(&skip=[0-9])\w+/g, "");
  if( !getSearchHistory() )
  console.error('no searchHistory')
  else
  if(docUrl != getSearchHistory()[0].url && docUrl.includes("listtype=imagelist")){

    /*

    let url = document.URL;

    let val_boendetyp = $(".objecttype>div>.input .checked").html();
    let val_visabara = $(".objecttype>div>.filterrequirements .checked").html();
    if(typeof val_visabara != "string") val_visabara = "";

    let val_hyra = $("#rent").val();

    let val_hyra_result = "";
    if(val_hyra.length > 1){
    val_hyra_result = `Hyra: ${val_hyra}<br>`
  }
  let val_kommun = $(".label label[for='city']").html();
  let val_antalrum = $(".label label[for='rooms']").html();
  let val_omrade = $(".label label[for='508A8CB406FE001F00030A60']").html();
  let val_typavlagenhet = $(".label label[for='objecttype']").html();


  */

  historyObj = {}
  historyObj.url = docUrl
  historyObj.val_boendetyp = $(".objecttype>div>.input .checked").html();
  historyObj.val_visabara = (typeof $(".objecttype>div>.filterrequirements .checked").html() != "string") ? "" : $(".objecttype>div>.filterrequirements .checked").html();
  historyObj.val_hyra = $("#rent").val();
  historyObj.val_hyra_result = (historyObj.val_hyra.length > 1) ? `Hyra: ${historyObj.val_hyra}<br>` : "";
  historyObj.val_kommun = $(".label label[for='city']").html();
  historyObj.val_antalrum = $(".label label[for='rooms']").html();
  historyObj.val_omrade = $(".label label[for='508A8CB406FE001F00030A60']").html();
  historyObj.val_typavlagenhet = $(".label label[for='objecttype']").html();

  if( JSON.stringify(historyObj) != JSON.stringify( getSearchHistory() ) )
  {
    let temp = getSearchHistory()
    temp.unshift( historyObj )
    setSearchHistory(temp)

    /*

    newRow(HISTORY[0].url,
    HISTORY[0].val_boendetyp,
    HISTORY[0].val_hyra_result,
    HISTORY[0].val_kommun,
    HISTORY[0].val_omrade,
    HISTORY[0].val_typavlagenhet,
    HISTORY[0].val_visabara);

    */


  }
}

/*
$("tr.imageitem").each( function(i, e){
$td = $(this).children("td[colspan='7']");

//Hyra
hyra=$(this).find(".rent").first().html();
$(this).append(`<td>${hyra}</td>`);
//Area
area=$(this).find(".area").first().html();
$(this).append(`<td>${area}</td>`);
//squaremeters
squaremeters=$(this).find(".squaremeters").first().html();
$(this).append(`<td>${squaremeters}</td>`);
//moveindate
moveindate=$(this).find(".moveindate").first().html();
$(this).append(`<td>${moveindate}</td>`);
//publishdate
publishdate=$(this).find(".publishdate").first().html();
$(this).append(`<td>${publishdate}</td>`);
//criteria
criteria=$(this).find(".criteria").first().html();
$(this).append(`<td>${criteria}</td>`);
//objectphoto
objectphoto=$(this).find(".objectphoto").first().html();
$(this).append(`<td class="objectphoto">${objectphoto}</td>`);

$( $td ).remove();
});
*/




} //function onSearch(){


  $(document).on('click','hr',function(){
    clearInterval(main)
    toast("stopped interval")
  });

  $(document).on('submit','form#objectsearchform',function(){
    console.log("test");
  });


  $(document).on('click', 'button[name="search"]', function(){

    setTimeout(function(){
      onSearch();
    }, 2000);

    /*
    var derp = true;
    while( derp ){

    if( $(".progress").attr("style") == "display: none;" ){
    derp = false;
    onSearch();
    console.log( $(".imagelist tbody").html() );
  }
}
*/

});



if( body && body.indexOf('INLOGGAD SOM') > -1 && getS("autolgn")) {
  toast('Inloggad');

}
function autolgn(){

  login();

  if( getS('usr') && getS('pas') ){

    if( body && body.indexOf('Logga in') > -1 ) {
      Materialize.toast('Loggar in', 2000);
      $.get( "https://nya.boplats.se/login/", function( data ) {

      }).done( function( data ) {

        var usr = getS('usr');
        var pwd = getS('pas');
        var at  = $( data ).find('input[name*="authenticity_token"]').val();

        $.post( "https://nya.boplats.se/login/fragment", { username: usr, password: pwd, utf8:"✓", authenticity_token:at }, function( data ) {

        }).done( function( data ) {
          if( body && body.indexOf('Inloggning startad') == -1 ) {
            location.reload();
          } else {
            Materialize.toast('Inloggningen misslyckades', 5000);

          }
        });
      });
    }
  }
} //function autolgn



function setS(a, b){
  localStorage.setItem( a.toString(), b.toString() );
  //console.log("setS("+a+") = " + b);
  return b;
}
function getS(a){
  savedVal = localStorage.getItem(a);
  //if(savedVal)
  return savedVal; //parseInt(localStorage.getItem(a));

  //else
  //	return setS(a, 0);
  //console.log("getS("+a+") = " + localStorage.getItem(a));
}
function loadS(set = false){
  cmsg = "";
  for(i=0;i<4;i++){
    if(set === true && i === 0) continue;

    e = $('#boplatshelper input')[i];
    eID = $(e).attr("id");

    if(getS(eID) == 1){




      //if(i == 0) continue;


      //console.log(eID + "("+i+") triggered.");
    }else{
      $(e).prop('checked', false);
      $("#" + eID).trigger("change");
      //console.log(eID + "("+i+") NOT triggered.");

    }
    cmsg += "\tloadS("+eID+") = " + getS(eID) + "\n ";
  }

  e = $("#autolgn");
  eID = $(e).attr("id");
  if( getS(eID) && getS("usr") && getS("pas")){
    $(e).prop('checked', true);
    $(e).trigger("change");
    vs = "";
    for(x=0;x<getS("usr").length;x++){vs+="*";}
    $("#usr").val( vs );
    xs = "";
    for(x=0;x<getS("pas").length;x++){xs+="*";}
    $("#pas").val( xs );
  }
  //console.log(cmsg);
}


/*
if( $(eID).is(":checked") ){
$("#padding").prop('checked', false);
$("#padding").trigger("change");
} else {
$("#padding").prop('checked', true);
$("#padding").delay(2000).trigger("change");

}*/

function getRangordning(){

  $(".imageitem td a").each( function(i, e){
    var href = $(e).attr("href");

    $(e).append(`<div class="rangbox">${LOADER}</div>`);
    var ed = $(e).children(".rangbox");

    $.get( href, function( data ) {

    }).done( function( data ) {
      let omraden = "";

      $( data ).find(".baseSearchCriteria li.red:not(.summary)").each( function(i, ee){
        let r = $(ee).html().includes("Områden är")

        if(r){
          let h = $(ee).html().replace("Områden är ", "");

          if( $( data ).find(".baseSearchCriteria li.red:not(.summary)").length == 1 ){
            color = `green`;
            title = `Om du lägger till detta område kan du sedan söka lägenheten.`
            STATUS_TXT += `${_frames()} Området <span style="color:green">${h}</span> krävs för ansökan.</div>`;

            if(LOCKED){
              omraden += `<br><span style="color:red">Dina områden är låsta för tillfället: ${LOCKED_TXT}</span>`;
            }
          } else {
            color = `red`;
            title = ``
          }

          omraden += `
          <div>
          <a href="https://nya.boplats.se/minsida/profil#sokerboende" target="_blank" style="text-decoration: none !important; color: ${color};" title="${title}">
          ${h}: ${title} (öppnas i ny flik)
          </a>
          </div>`;


        }

      });


      var sd = $( data ).find("#sortingDetails").html();

      if( data && data.indexOf('Ingen rangordning.') > -1 ) {

        $(ed).html(`
          <section class="rangordning-display">
          <strong>Rangordning: </strong>Ingen, per profil. (${sd})<br>
          ${omraden}
          </section>
          `);

          // $(ed).addClass("cx");

        } else {
          $(ed).html(`
            <section class="rangordning-display">
            <strong>Rangordning: </strong>Dagar hos Boplats (${sd})<br>
            ${omraden}
            </section>
            `);
            // $(ed).addClass("cy");
          }

        });

      });

    }

    //$("table.status").on("change", "input", function(event){
    $(document).on("change", "table.status input", function(event){
      var id = $(this).attr("id");
      var sID = id;
      var $this = this;

      if ( this.checked ){ setS(sID, 1); } else { setS(sID, 0); }

      switch(id){
        case "savesettings":
        if ( $this.checked && getS(sID) == 1){
          loadS(true);
        }

        break;
        case "unavailable":

        if ( this.checked ){

          $(".imageitem td a").each( function(i, e){
            if( $(e).find('img[src*="cross"]').length ){
              $(this).parent().parent().addClass("hiddenAvail");

            }
          });


        } else {
          $(".imageitem td a").each( function(i, e){
            if( $(e).find('img[src*="cross"]').length ){
              $(this).parent().parent().removeClass("hiddenAvail");

            }
          });
        }
        break;
        case "applied":
        if ( this.checked ){

          $(".imageitem td a").each( function(i, e){
            if( $(e).find('img[src*="ansokt"]').length ){
              $(this).parent().parent().addClass("hiddenApplied");
            }
          });

        } else {
          $(".imageitem td a").each( function(i, e){
            if( $(e).find('img[src*="ansokt"]').length ){
              $(this).parent().parent().removeClass("hiddenApplied");
            }
          });

        }
        break;
        case "padding":
        if ( this.checked )
        {
          $('.list.imagelist td').addClass("nopadding");

        }
        else
        {
          $('.list.imagelist td').removeClass("nopadding");
        }
        break;
        case "rangordning":
        if ( this.checked ){

          if( $(".rangbox").length > 0)
          {
            $(".rangbox").toggle();
          }
          else
          {

            getRangordning()


          }


        } else {
          $(".rangbox").hide();

        }
        break;
        case "applytoall":
        $(".imageitem td a").each( function(i,e){

          var l = $(this).attr("href");

          if( !$(e).find('img[src*="cross"]').length && !$(e).find('img[src*="ansokt"]').length ){
            $this = $(this);
            ansokTillAnnons($this);

          } else {
            return true;
          }
        });
        break;
        case "usr":

        setS(sID, $(this).val() );
        //Materialize.toast('Användarnamnet "'+$(this).val()+'" sparades', 2000);

        break;
        case "pas":

        setS(sID, $(this).val() );
        //Materialize.toast('Lösenordet sparades', 2000);

        break;
        case "autolgn":
        if ( this.checked ){
          autolgn();

        }
        break;
      }
    }); // $("table.status").on("change", "input", function(event){

      window.sokta = 0;
      function ansokTillAnnons($this){

        var href = $this.attr("href");
        var statusImg = $this.find('img[alt="Du uppfyller basvillkoren"]');
        $this.toggleClass("curr");

        $(statusImg).replaceWith(LOADER);

        $.post( href, { apply: "apply" }, function( data ) {

        }).done( function( data ) {
          if( data && data.indexOf('Intresseanmälan har registrerats') > -1 ) {
            $(".curr").find('img[alt="Laddar"]').parent().replaceWith('<p><img src="https://nya.boplats.se/static/ansokt.png" alt="Du har ansökt"></p>');
            $this.toggleClass("curr");
            window.sokta++;

            $this.find('div.applybox.boxy').hide();
          } else {
            $(".curr").find('img[alt="Laddar"]').parent().replaceWith('<p><img src="https://nya.boplats.se/static/result_cross.png" alt="Du uppfyller inte basvillkoren"><div style="margin-left:5px;font-size: smaller;font-weight: 600;text-transform: uppercase;text-align: center;">kan ej ansöka</p>');
            $this.toggleClass("curr");
            $this.find('div.applybox.boxy').hide();
          }


        });

        console.log(´Uppfyller basvilkoren?´, $('img[alt="Du uppfyller basvillkoren"]').length );
        console.log(´window.sokta:´, window.sokta );


        if( $('img[alt="Du uppfyller basvillkoren"]').length == window.sokta){
          $("#sokta").html( ' (Alla annonser är sökta!</span> <img src="http://2.bp.blogspot.com/-OsnLCK0vg6Y/UZD8pZha0NI/AAAAAAAADnY/sViYKsYof-w/s1600/big-smile-emoticon-for-facebook.png">');
        } else {
          $('#sokta').html(' ('+ sokta +' lägenter sökta)');
        }

      } //function ansokTillAnnons($this)

      applyboxClick = $(document).on("click", ".applybox", function(e){
        e.preventDefault();
        var that = $(this).parent("div").parent("a");
        ansokTillAnnons( that );
      });


      $(document).on("click", "#minimaxi", function(e){
        $("#boplatshelper div").each( function(i, ee){
          if(i > 1){
            $(this).toggle();
          }
        });
      });


    }); // .ready()
  });
})(jQuery);

function tableTitle(str){ return `<tr><td colspan="4"><h4>${str}</h4></td></tr>`;}

BOPLATS_HELPER_HTML = `
<table id="boplats_helper_html" class="status">
<thead>
${tableTitle('Boplats Helper')}
</thead>
<tbody>
${tableTitle('Boplats Helper')}
<!--
<tr>
<td><input type="checkbox" id="savesettings"></td>
<th><label for="savesettings">Spara inställningarna som du gör här?		<span id='0'></span></label></th>
</tr>
-->

<tr class="unavailable" style="display:none;">
<td><input type="checkbox" id="unavailable" ></td>
<th><label for="unavailable">Göm annonser som du ej uppfyller villkoren för			<span id='1'></span></label></th>
</tr>
<tr class="applied" style="display:none;">
<td><input type="checkbox" id="applied"     ></td>
<th><label for="applied">Göm sökta annonser								<span id='2'></span></label></th>
</tr>
<!--
<tr>
<td><input type="checkbox" id="padding"     ></td>
<th><label for="padding">Göm mellanrummet mellan varje annons				<span id='3'></span></label></th>
</tr>
-->
<tr class="rangordning" style="display:none;">
<td><input type="radio" id='rangordning'></td>
<th><label for="rangordning">Visa rangordning för varje annons			<span id='4'></span></label></th>
</tr>
<tr class="applytoall" style="display:none;">
<td><input type="radio" id='applytoall'     ></td>
<th><label for="applytoall">Sök alla lägenheter <span id='sokta'></span>	<span id='5'></span></label></th>
</tr>
</tbody>
</table>

<table id="autologin" class="status">
<thead>
${tableTitle('Autoinloggning')}
</thead>
<tbody>
${tableTitle('Autoinloggning')}
<tr class="autologin">
<td colspan="4">
<label for="5">Användarnamn:<span id="5"></span></label>
<input type="text" id="usr"  name="5">
</td>
</tr>
<tr class="autologin">
<td colspan="4">
<label for="6">Lösenord:<span id="6"></span></label>
<input type="password" id="pas" name="6">
</td>
</tr>
<tr class="autologin">
<td colspan="4">
<button type="submit" name="login_button" class="notable">Logga in</button>
</td>
</tr>
<tr class="autologin">
<td><input type="checkbox" id="autolgn" name="7"></td>
<th><label for="7">Bli inloggad automatiskt om du har blivit utloggad?<span id="7"></span></label></th>
</tr>
</tbody>
</table>

<table id="status-log" class="status">
<thead>
${tableTitle('Logg')}
</thead>
<tbody>

<tr id="status_txt">
<td></td>
</tr>
</tbody>
</table>

<table id="search-history" class="status">
<thead>
${tableTitle('Sökhistorik')}
</thead>
<tbody>

</tbody>
</table>
`;
